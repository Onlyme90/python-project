from math import sqrt
print("quadratic eq :(a*x^2)+b*x+c")
a=float(input("a: "))
b=float(input("b: "))
c=float(input("c: "))
d=b**2-4*a*c
if d>0:
	nr=2
	x1=((-b)+sqrt(d))/(2*a)
	x2=((-b)+sqrt(d))/(2*a)
	print("There are two roots: %f and %f"%(x1,x2))
elif d==0:
	nr=1
	x=(-b)/(2*a)
	print("There is one root:",x)
else:
	nr=0
	print("no roots,discriminant<0")